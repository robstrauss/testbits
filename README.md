# README #

Two small and straightforward Python3.5 applications, run from the command line, testing connection, 
insert data and returning data. SQLiteConnect.py connecting and insert a Name and Surname into a SQLite database.
The other application SQLiteResults.py returning the Resultset to the command line. Written on Zorin 12.1,
 so I won't know how it will behave or work on Windows.